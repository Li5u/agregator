import time
import random

import requests
from bs4 import BeautifulSoup


class RealtFlatPageParser:
    def __init__(self):
        self.header = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36'}

    def __get_html(self, url):
        time.sleep(random.uniform(1, 3))
        r = requests.get(url, headers=self.header)
        return r.content

    def parse(self, url):
        html = self.__get_html(url)
        soup = BeautifulSoup(html, 'html.parser')
        data = {'url': url,
                'address': {},
                'owner': {},
                'date': {}
                }
        trs = soup.find_all('tr', class_='table-row')
        for tr in trs:
            if tr.find('td', class_='table-row-left').text == 'Населенный пункт':
                city = tr.find('td', class_='table-row-right').text
                data['address']['city'] = city
                continue

            if tr.find('td', class_='table-row-left').text == 'Адрес':
                street_house = tr.find('td', class_='table-row-right').text.split(',')
                data['address']['street'] = street_house[0].split()[0]
                try:
                    data['address']['house'] = street_house[1]
                except:
                    data['address']['house'] = '?'

                data['address']['house'] = data['address']['house'].replace(' Информация о доме', '')
                continue

            if tr.find('td', class_='table-row-left').text == 'Телефоны':
                phones = []
                ases = tr.find_all('a')
                for a in ases:
                    phones.append(a.get('href').split(':')[-1])
                data['phones'] = phones
                continue

            if tr.find('td', class_='table-row-left').text == 'Ориентировочная стоимость эквивалентна':
                price = tr.find('span', class_='price-byr').text
                edited_price = ''
                for char in price:
                    if char.isdigit():
                        edited_price += char
                data['price'] = edited_price
                continue

            if tr.find('td', class_='table-row-left').text == 'Комнат всего/разд.':
                number_of_rooms = tr.find('strong').text.split('/')[0]
                data['rooms_count'] = number_of_rooms
                continue

            if tr.find('td', class_='table-row-left').text == 'Контактное лицо':
                data['owner']['name'] = tr.find('td', class_='table-row-right').text
                continue

            if tr.find('td', class_='table-row-left').text == 'Агентство':
                data['owner']['agency'] = tr.find('td', class_='table-row-right').text
                continue

            if tr.find('td', class_='table-row-left').text == 'Бытовая техника':
                data['appliances'] = tr.find('td', class_='table-row-right').text
                continue

            if tr.find('td', class_='table-row-left').text == 'Дополнительно':
                data['extra_info'] = tr.find('td', class_='table-row-right').text
                continue

            if tr.find('td', class_='table-row-left').text == 'E-mail':
                data['email'] = tr.find('td', class_='table-row-right').text.replace('(собачка)', '@')

            if tr.find('td', class_='table-row-left').text == 'Дата обновления':
                date = tr.find('td', class_='table-row-right').text.split('-')
                try:
                    data['date']['year'] = date[0]
                    data['date']['month'] = date[1]
                    data['date']['day'] = date[2]
                except: pass
                continue
        print('+1')
        return data
