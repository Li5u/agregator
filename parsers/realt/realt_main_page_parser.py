import time
import random

from bs4 import BeautifulSoup
import requests


class RealtFlatListParser:
    def __init__(self):
        self.url = 'https://realt.by/rent/flat-for-long/'
        self.header = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36'}

    def get_html(self, url):
        time.sleep(random.uniform(1, 3))
        r = requests.get(url, headers=self.header)
        return r.content

    def parse(self):
        html = self.get_html(self.url)
        soup = BeautifulSoup(html, 'html.parser')
        try:
            number_of_pages = int(soup.find('div', class_='uni-paging').find_all('a')[-1].text)
        except:
            number_of_pages = 1

        flats_links = []
        for page in range(0, number_of_pages):
            if page == 0:
                active_url = self.url
            else:
                active_url = self.url + '?page=' + str(page)
            html = self.get_html(active_url)
            soup = BeautifulSoup(html, 'html.parser')
            all_flats = soup.find_all('div', class_='title')
            for flat in all_flats:
                link = flat.find('a').get('href')
                flats_links.append(link)
        print(len(flats_links))
        return flats_links
