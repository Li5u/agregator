import time
import random

import requests
from bs4 import BeautifulSoup


class HataFlatListParser:
    def __init__(self):
        self.header = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36'}
        self.url = 'https://www.hata.by/rent-flat/'
        self.mains = [self.url]

    def __get_html(self, url):
        time.sleep(random.uniform(1, 3))
        r = requests.get(url, headers=self.header)
        return r.content

    def __get_mains(self):
        current_url = self.url
        while True:
            html = self.__get_html(current_url)
            soup = BeautifulSoup(html, 'html.parser')
            try:
                self.mains.append(soup.find('a', class_='next').get('href'))
                current_url = soup.find('a', class_='next').get('href')
            except:
                break

    def parse(self):
        self.__get_mains()
        flat_links = []
        for page in self.mains:
            html = self.__get_html(page)
            soup = BeautifulSoup(html, 'html.parser')
            h3_titles = soup.find_all('h3', class_='title')
            for h3 in h3_titles:
                flat_links.append(h3.find('a', target='_blank').get('href'))
        return flat_links



