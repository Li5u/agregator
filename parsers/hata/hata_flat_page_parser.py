import time
import random

import requests
from selenium import webdriver
from bs4 import BeautifulSoup


class HataFlatPageParser:
    def __init__(self):
        self.header = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36'}
        self.chromedriver = 'C:/Users/Li5u/Downloads/chromedriver_win32/chromedriver.exe'
        self.driver = webdriver.Chrome(self.chromedriver)
        self.__login()
        self.current_course = self.__get_course()

    def __login(self):
        self.driver.get('https://www.hata.by/rent-flat/')
        self.driver.find_element_by_class_name('btn-xl').click()
        self.driver.find_element_by_name("login-input").send_keys('dimkadubovik99@mail.ru')
        self.driver.find_element_by_name("login-password").send_keys('24514125dds99')
        self.driver.find_element_by_xpath("//div[@class='section section_small']").click()

    def __get_html(self, url):
        # time.sleep(random.uniform(1, 3))
        r = requests.get(url, headers=self.header)
        return r.content

    def __get_phone(self, url):
        self.driver.get(url)
        self.driver.find_element_by_class_name('show-phone').click()
        phone = ''
        phones = []
        while not phone:
            phone = self.driver.find_element_by_xpath('//div[@class="contacts-phones"]').text
            try:
                self.driver.find_element_by_class_name('show-phone').click()
            except: pass
        for element in ['(', ')', '-', ' ']:
            phone = phone.replace(element, '')
        phones.extend(phone.split('\n'))
        return phones

    def __get_course(self):
        html = self.__get_html('https://myfin.by/bank/kursy_valjut_nbrb')
        soup = BeautifulSoup(html, 'html.parser')
        course = soup.find('tbody', class_='table-body').find('tr').find('td', class_='course').text
        return float(course)

    def parse(self, url):
        print(url)
        html = self.__get_html(url)
        soup = BeautifulSoup(html, 'html.parser')
        data = {'address': {},
                'url': url,
                'date': {}}

        address = soup.find('div', class_='b-card__address').find('span').text.split('.')
        try:
            data['address']['city'] = address[1].split(',')[0]
            data['address']['street'] = address[2].split()[0]
            data['address']['house'] = address[3].lstrip()
        except IndexError:
            print('wtf')

        data['phones'] = self.__get_phone(url)

        price = soup.find('div', class_='value').text.split()[0]
        data['price'] = round(int(price) * self.current_course)

        trs = soup.find('table', class_='i-table').find_all('tr')
        for tr in trs:
            if tr.find_all('td')[0].text == 'Комнат':
                rooms = tr.find_all('td')[1].find('a').text
                data['rooms_count'] = rooms

        tables = soup.find_all('table', class_='i-table')
        trs = []
        for table in tables:
            trs.extend(table.find_all('tr'))
        for tr in trs:
            tds = tr.find_all('td')
            if tds[0].text == 'Дата обновления':
                date = tds[1].text.split('.')
                data['date']['year'] = date[2]
                data['date']['month'] = date[1]
                data['date']['day'] = date[0]

        a = soup.find('div', class_='contacts').text.split(':')
        email = a[1].replace(' ', '').split('\n')[0]
        data['email'] = email

        data['extra_info'] = soup.find('div', class_='description').text

        return data






