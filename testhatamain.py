import pymongo as pm

from database.datawriter import DataWriter
from parsers.hata.hata_main_page_parser import HataFlatListParser
from parsers.hata.hata_flat_page_parser import HataFlatPageParser

client = pm.MongoClient("localhost", 27017)
db = client['HataFlatsAgregator']
ads_coll = db['ads']
old_ads = db['old_ads']
flats_coll = db['flats']
phones_coll = db['phones']

fl = HataFlatListParser()
fp = HataFlatPageParser()
wr = DataWriter(flats_coll, ads_coll, old_ads, phones_coll)

flats = fl.parse()
for flat in flats:
    flat_data = fp.parse(flat)
    wr.write_into_mongo(flat_data)
