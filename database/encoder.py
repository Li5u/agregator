from models.flat import Flat
from models.ad import Ad


class Encoder:
    def encode_flat(self, flat_object):
        return {'_type': 'flat',
                'address': flat_object.get_address(),
                'price': flat_object.get_price(),
                'rooms_count': flat_object.get_rooms_count(),
                'extra_info': flat_object.get_extra(),
                'appliances': flat_object.get_appliances()}

    def encode_ad(self, ad_object):
        return {'_type': 'ad',
                'url': ad_object.get_url(),
                'flat': ad_object.get_flat(),
                'previous': ad_object.get_previous(),
                'name': ad_object.get_name(),
                'phones': ad_object.get_phones(),
                'email': ad_object.get_email(),
                'date': ad_object.get_date()}

    def decode_flat(self, document):
        assert document["_type"] == "flat"
        return Flat(document).__dict__

    def decode_ad(self, document):
        assert document["_type"] == "ad"
        return Ad(document, document['flats']).__dict__
