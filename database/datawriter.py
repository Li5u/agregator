import pymongo as pm

import database.encoder as enc


class DataWriter:
    def __init__(self, flats_collection, ads_collection, old_ads_collection, phones_collections):
        self.flats = flats_collection
        self.ads = ads_collection
        self.old_ads = old_ads_collection
        self.phones = phones_collections
        self.encoder = enc.Encoder()

    def write_into_mongo(self, data_dict):
        current_flat = enc.Flat(data_dict)
        if self.flats.count_documents(current_flat.__dict__) == 0:
            ins_flat = self.flats.insert_one(self.encoder.encode_flat(current_flat))
            current_flat_id = ins_flat.inserted_id
        else:
            current_flat_id = self.flats.find(current_flat.__dict__, {'_id': 1})[0]['_id']
        current_ad = enc.Ad(data_dict, current_flat_id)
        last_usage = self.ads.find_one({'url': data_dict['url']})
        if not last_usage:
            last_usage = self.old_ads.find_one({'url': data_dict['url']}, sort=[('_id', pm.DESCENDING)])

        if last_usage:
            dict_to_compare = current_ad.__dict__
            del dict_to_compare['previous']
            if last_usage == self.ads.find_one(dict_to_compare):
                print('pass')
            else:
                current_ad.previous = last_usage['_id']
                self.ads.insert_one(self.encoder.encode_ad(current_ad))
                self.old_ads.insert_one(last_usage)
                self.ads.delete_one(last_usage)
        else:
            self.ads.insert_one(self.encoder.encode_ad(current_ad))

    def clear_ads(self, realt_flat_links):
        ads_to_delete = self.ads.find({'url': {'$in': realt_flat_links}})
        for ad in ads_to_delete:
            self.old_ads.insert_one(ad)
            self.ads.delete_one(ad)