class Ad:
    def __init__(self, data_dict, flat_id):
        self.previous = ''
        self.flat = flat_id
        self.name = None
        self.phones = []
        self.date = None
        self.email = None
        self.url = data_dict['url']

        try:
            self.name = data_dict['owner']
        except:
            self.name = ''

        try:
            self.phones = data_dict['phones']
        except:
            self.phones = []

        try:
            self.date = data_dict['date']
        except:
            self.date = ''

        try:
            self.email = data_dict['email']
        except:
            self.email = ''

    def get_flat(self):
        return self.flat

    def get_name(self):
        return self.name

    def get_phones(self):
        return self.phones

    def get_email(self):
        return self.email

    def get_date(self):
        return self.date

    def get_url(self):
        return self.url

    def get_previous(self):
        return self.previous
