class Flat:
    def __init__(self, data_dict):
        self.address = None
        self.price = None
        self.rooms_count = None
        self.extra_info = None
        self.appliances = None
        try:
            self.address = data_dict['address']
        except:
            self.address = ''
        try:
            self.rooms_count = int(data_dict['rooms_count'])
        except:
            self.rooms_count = 0
        try:
            self.price = int(data_dict['price'])
        except:
            self.price = 0
        try:
            self.appliances = data_dict['appliances']
        except:
            self.appliances = ''
        try:
            self.extra_info = data_dict['extra_info']
        except:
            self.extra_info = ''

    def get_address(self):
        return self.address

    def get_price(self):
        return self.price

    def get_rooms_count(self):
        return self.rooms_count

    def get_extra(self):
        return self.extra_info

    def get_appliances(self):
        return self.appliances


