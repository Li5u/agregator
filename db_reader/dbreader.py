from bson import ObjectId


class DBReader:
    def __init__(self, db):
        self.min_price = 0
        self.max_price = 100000
        self.rooms_count = None
        self.street = None
        self.db = db

    def get_min_price(self, min_price):
        self.min_price = min_price

    def get_max_price(self, max_price):
        self.max_price = max_price

    def get_rooms_count(self, rooms_count):
        self.rooms_count = rooms_count

    def get_street(self, street):
        self.street = street

    def find_flats(self):
        active_flats_id = []
        for ad in self.db['ads'].find({}, {'flat': 1}):
            active_flats_id.append(ad['flat'])

        parameters = {'price': {'$gt': self.min_price,
                                '$lt': self.max_price},
                      '_id': {'$in': active_flats_id}}
        if self.rooms_count:
            parameters['rooms_count'] = {'$eq': self.rooms_count}
        if self.street:
            parameters['address.street'] = {'$eq': self.street}

        flats = self.db['flats'].find(parameters)
        ads = []
        for flat in flats:
            ads.append((self.db['ads'].find_one({'flat': {'$eq': ObjectId(flat['_id'])}})))
        return ads


