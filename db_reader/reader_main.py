import pymongo as pm

from db_reader.dbreader import DBReader


client = pm.MongoClient("localhost", 27017)
db = client['FlatsAgregator']
ads_coll = db['ads']
old_ads = db['old_ads']
flats_coll = db['flats']
phones_coll = db['phones']

reader = DBReader(db)
# reader.get_min_price(1000)
# reader.get_max_price(2000)
# reader.get_rooms_count(3)
# reader.get_street('Пулихова')
for ad in reader.find_flats():
    print(ad)