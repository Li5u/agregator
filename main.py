import pymongo as pm

from database.datawriter import DataWriter
from parsers.realt.realt_main_page_parser import RealtFlatListParser
from parsers.realt.realt_flat_page_parser import RealtFlatPageParser

client = pm.MongoClient("localhost", 27017)
db = client['FlatsAgregator']
ads_coll = db['ads']
old_ads = db['old_ads']
flats_coll = db['flats']
phones_coll = db['phones']

fl = RealtFlatListParser()
fp = RealtFlatPageParser()
wr = DataWriter(flats_coll, ads_coll, old_ads, phones_coll)

flats = fl.parse()
for flat in flats:
    flat_data = fp.parse(flat)
    wr.write_into_mongo(flat_data)
